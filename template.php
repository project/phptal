<?php
/**
 *
 */
function phptal_created(&$node) {
  return t('by %a on %b', array(
    '%a' => theme('username', $node), '%b' => format_date($node->created)));
}

function phptal_node_class($sticky, $zebra, $status, $teaser, $type='node') {
  $class = 'node ' . $type;
  $class .= $sticky ? '-sticky' : '';
  $class .= $zebra == 'odd' ? ' odd' : ' even';
  $class .= !$status ? " node-unpublished" : '';
  $class .= $teaser ? " node-teaser" : '';

  return $class;  
}

function phptal_block_class($block) {
  return "region-$block->region block block-$block->module";
}

function phptal_block_id($block) {
  return "block-$block->module-$block->delta";
}

function phptal_permalink($node_url) {
  return sprintf('<a href="%s" title="%s">#</a>', $node_url, 
    check_plain(t('Copy this link as the URL of this article')));
}

function phptal_comment_class($new) {
  return 'comment' . ($new ? ' comment-new' : ''); 
}

