********************************************************************
                D R U P A L   THEME  ENGINE                     
********************************************************************
Name: phptal
Version: 3.0
Author: Olav Schettler
Email: olav-phptal at contaire dot net
Last update: 02 February 2007
Drupal: 5.x

********************************************************************
DESCRIPTION:

This is a theme engine for Drupal 5.x, which allows the use
of templates written in the PHPTAL language. This engine is a nearly
one-to-one copy of the phptemplate engine, just replacing the underlying
template engine with the one from <http://phptal.sourceforge.net/>.

To be used, the engine needs an installation of PHPTAL in this directory.
It was developed using PHPTAL 1.1.4 stable.

I believe that this engine provides the same advantage of a combined
push/pull approach for dynamic content as PHPtemplate but at the same
time the template files are clean and pure XHTML. PHPTAL templates can
thus be edited with most standard WYSIWYG HTML editors and are
guaranteed to output valid XHTML.

PHPTAL works - amonst others - with the proto-template box_gray by Adrian 
Simmons for PHPTemplate, ported to PHPTAL and renamed to tal_gray.

A complete documentation on the PHPTAL template language can be found
at <http://phptal.sourceforge.net/>. There you also find links to the
many resources for the Python-based TAL implementation. 

********************************************************************
SYSTEM REQUIREMENTS:

Drupal: Drupal 5.x

********************************************************************
INSTALLATION:

The recommended installation for PHPTAL is to use the Pear module.
This approach is explained on the PHPTAL page at
http://phptal.motion-twin.com/.

Download and install the tal_gray theme.

********************************************************************
SPECIAL THANKS :

Laurent Bedubourg for his implementation of PHPTAL.

********************************************************************
********************************************************************

This directory also contains a port of the andreas01 design
by Andreas Viklund. This was previously available as the 'olav' theme
but has been made the default theme for the phptal.engine now.
